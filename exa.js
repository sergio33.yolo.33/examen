function validar_edad() {
	var input_edad = document.getElementById('input_edad').value;
	const contenido = document.getElementById('etiqueta');
	if (input_edad >= 0 && input_edad <= 14) {
		var etiqueta = '<p style="color: #0023FE;"> Eres un nino </p>';
		contenido.innerHTML = etiqueta;
	}else if (input_edad >= 15 && input_edad <= 18) {
		var r = '<p style="color: #00FE36;"> Eres un joven </p>';
		contenido.innerHTML = r;
	}else if (input_edad >= 19 && input_edad <= 60) {
		var p = '<p style="color: #FE9600;"> Eres un adulto </p>';
		contenido.innerHTML = p;
	}else if (input_edad >= 61 && input_edad <= 99){
		var s = '<p style="color: #FE0000;"> Eres un adulto mayor </p>';
		contenido.innerHTML = s;
	}
}